Materialize Theme  for YII2
===========================
This extension will need to replace the yii2-bootstrap

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require socialist/yii2-materialize:dev-master
```

or add

```
"socialist/yii2-materialize": "@dev"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?php
use socialist\materialize;

MaterializeAsset::register($this);

?>```