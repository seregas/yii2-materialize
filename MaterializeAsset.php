<?php

namespace socialist\materialize;

use yii\web\AssetBundle;

/**
* MaterializeAsset
*/
class MaterializeAsset extends AssetBundle
{
	public $sourcePath = '';

	public $css = [
	];

	public $js = [
	];

	public $depends = [
		'yii\web\YiiAsset',
	];
}
